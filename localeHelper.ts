import { GetServerSidePropsContext } from 'next';
import nookies, { setCookie } from 'nookies';

export function getLocale(context: GetServerSidePropsContext): string {
  const cookies = nookies.get(context);
  return cookies.locale ?? context.locale;
}

export function setLocale(newLocale: string): void {
  setCookie(null, 'locale', newLocale, {
    sameSite: 'Strict',
  });
}
