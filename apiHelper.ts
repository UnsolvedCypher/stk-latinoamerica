export function parseResult(result: string) {
  const resultNum = Number.parseFloat(result);
  const formattedResult = `${Math.floor(resultNum / 60)}:${(resultNum % 60).toFixed(3).padStart(6, '0')}`;
  return formattedResult;
}