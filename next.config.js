const { i18n } = require('./next-i18next.config');

module.exports = {
  i18n,
  env: {
    dbLocation: `/home/${process.env.USER}/stkservers.db`,
    speedrunAddonsFile: `/home/${process.env.USER}/addons-list.txt`,
    arTable: 'recordsAR',
    brTable: 'recordsBR',
    miTable: 'recordsMIAMI',
    allTable: 'recordsLATAM',
  },
  async redirects() {
    return [
      {
        source: '/ranked',
        destination: 'https://www.dropbox.com/s/1g8h7ptnfkwljge/ranked.zip?dl=1',
        permanent: false,
      },
      {
        source: '/bienavalidas',
        destination: 'https://www.dropbox.com/s/rl8rremrvk8y4bf/bienavaliadas.zip?dl=1',
        permanent: false,
      },
    ]
  },
};
