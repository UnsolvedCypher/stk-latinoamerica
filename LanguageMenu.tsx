import { Dropdown, Menu } from 'semantic-ui-react';
import Router from 'next/router';
import React from 'react';
import { setLocale } from './localeHelper';

export default function LanguageMenu(props: { language: string }): React.ReactElement {
  return (
    <Menu.Menu position="right">
      <Dropdown item text={props.language === 'es' ? 'Idioma: Español' : 'Língua: Português'}>
        <Dropdown.Menu>
          <Dropdown.Item
            content="Español"
            active={props.language === 'es'}
            onClick={() => {
              setLocale('es');
              Router.replace(Router.asPath, undefined, { scroll: false });
            }}
          />
          <Dropdown.Item
            content="Português"
            active={props.language === 'pt'}
            onClick={() => {
              setLocale('pt');
              Router.replace(Router.asPath, undefined, { scroll: false });
            }}
          />
        </Dropdown.Menu>
      </Dropdown>
    </Menu.Menu>
  );
}
