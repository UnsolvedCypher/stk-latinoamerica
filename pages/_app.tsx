/* eslint-disable react/jsx-props-no-spreading */
import Head from 'next/head';
import Link from 'next/link';
import '../style.css';
import {
  Menu, Container, Segment, List, Dropdown, Message, Button,
} from 'semantic-ui-react';
import React, { useState } from 'react';
import { AppProps } from 'next/app';
import { appWithTranslation, useTranslation } from 'next-i18next';
import LanguageMenu from '../LanguageMenu';
import { proximoCampeonato, urlParaCampeonato } from '../todosCampeonatos';

function MyApp({ Component, pageProps }: AppProps) {
  const { t, i18n } = useTranslation('common');
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <>
      <Head>
        <title>STK Latinoamérica</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />
      </Head>
      <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <Menu fluid fixed="top" inverted className="standard-navbar">
          <Container>
            <Link href="/">
              <Menu.Item as="a" header>
                STK Latinoamérica
              </Menu.Item>
            </Link>
            <Link href="/records"><Menu.Item as="a" content={t('records')} /></Link>
            <Link href="/records/speedrun"><Menu.Item as="a" content={t('speedrun')} /></Link>
            {/* <Link href="/campeonatos"><Menu.Item as="a" content="Campeonatos" /></Link> */}
            <LanguageMenu language={i18n.language} />
          </Container>
        </Menu>
        <Segment
          inverted
          style={{
            padding: 0,
            position: 'fixed',
            top: 0,
            margin: 0,
            marginTop: '-5px',
            paddingTop: '5px',
            width: '100%',
            zIndex: 10,
          }}
        >
          <Menu inverted borderless className="mobile-navbar">
            <Menu.Item icon="bars" onClick={() => setMenuOpen(!menuOpen)} />
            <Link href="/">
              <Menu.Item as="a" header>
                STK Latinoamérica
              </Menu.Item>
            </Link>
            <LanguageMenu language={i18n.language} />
          </Menu>
          {menuOpen && (
          <Menu fluid inverted vertical onClick={() => setMenuOpen(false)}>
            <Link href="/records"><Menu.Item as="a" content={t('records')} /></Link>
            <Link href="/records/speedrun"><Menu.Item as="a" content={t('speedrun')} /></Link>
          </Menu>
          ) }
        </Segment>
        <Container className="main-container" text style={{ marginTop: '7em', flexGrow: 1 }}>
          <Component {...pageProps} />
        </Container>
        <Segment inverted vertical style={{ margin: '2em 0em 0em', padding: '2em 0em' }}>
          <Container textAlign="center">
            <List horizontal inverted divided link size="small">
              <List.Item>
                {t('created_by_1')}
                {' '}
                <a href="https://supertuxkart.net">SuperTuxKart</a>
                {' '}
                {t('created_by_2')}
                {' ❤'}
              </List.Item>
            </List>
          </Container>
        </Segment>
      </div>
    </>
  );
}

export default appWithTranslation(MyApp);
