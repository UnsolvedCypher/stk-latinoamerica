/* eslint-disable jsx-a11y/label-has-associated-control */
import { withTranslation, WithTranslation } from 'next-i18next';
import React from 'react';
import {
  Button, Form, Header, Modal, TextArea, Label,
} from 'semantic-ui-react';
import Router from 'next/router';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { getLocale } from '../../../localeHelper';
import campeonatos, { Campeonato, urlParaCampeonato } from '../../../todosCampeonatos';

interface RegisterProps extends WithTranslation {
  campeonato: Campeonato;
}

interface RegisterState {
  done: boolean;
  comentarios: string;
  username: string;
}

class Register extends React.Component<RegisterProps, RegisterState> {
  constructor(props: RegisterProps) {
    super(props);
    this.state = {
      done: false,
      username: '',
      comentarios: '',
    };
  }

  render(): React.ReactNode {
    // const { t } = this.props;
    const {
      username, comentarios,
    } = this.state;
    return (
      <>
        <Header as="h1">
          Registrar para
          {' '}
          {this.props.campeonato.nombre}
        </Header>
        <Label
          content={new Date(this.props.campeonato.fecha).toLocaleString(undefined, { timeZoneName: 'short' })}
          color="blue"
          icon="calendar"
        />
        <br />
        <br />
        <Form>
          <Form.Field required>
            <label>Usuario de STK</label>
            <input
              placeholder="Username"
              onChange={(e) => this.setState({ username: e.target.value })}
            />
          </Form.Field>
          <Form.Field>
            <label>Comentarios adicionales</label>
            <TextArea
              onChange={(e) => this.setState({ comentarios: e.target.value })}
            />
          </Form.Field>
          <Button
            type="submit"
            primary
            disabled={username.length === 0}
            onClick={async () => {
              await fetch('/api/respuestas', {
                method: 'POST',
                body: JSON.stringify({
                  campeonato: this.props.campeonato.nombre,
                  username,
                  comentarios,
                  fecha: new Date().toISOString(),
                }),
              });
              this.setState({ done: true });
            }}
          >
            Submit
          </Button>
        </Form>
        <Modal
          open={this.state.done}
          size="small"
        >
          <Header icon>
            Gracias
          </Header>
          <Modal.Content>
            <p style={{ textAlign: 'center' }}>
              ¡Gracias! Nos vemos pronto
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={() => Router.push('/')} content="Ok" />
          </Modal.Actions>
        </Modal>
      </>
    );
  }
}

export const getServerSideProps: GetServerSideProps = async (context) => ({
  props: {
    ...(await serverSideTranslations(getLocale(context), ['common'])),
    campeonato: campeonatos.find((c) => urlParaCampeonato(c) === context.params.id),
  },
});

export default withTranslation('common')(Register);
