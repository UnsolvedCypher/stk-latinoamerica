/* eslint-disable jsx-a11y/label-has-associated-control */
import { withTranslation, WithTranslation } from 'next-i18next';
import React from 'react';
import {
  Button, Form, Header, Modal, TextArea,
} from 'semantic-ui-react';
import Router from 'next/router';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { getLocale } from '../localeHelper';

interface RegisterState {
  done: boolean;
  server: string;
  sugerencias: string;
  username: string;
}

class Register extends React.Component<WithTranslation, RegisterState> {
  readonly formName = 'Campeonato 2021-06-04';

  constructor(props: WithTranslation) {
    super(props);
    this.state = {
      done: false,
      username: '',
      server: '',
      sugerencias: '',
    };
  }

  render(): React.ReactNode {
    // const { t } = this.props;
    const {
      username, server, sugerencias,
    } = this.state;
    const { formName } = this;
    return (
      <>
        <Header as="h1">Registrar</Header>
        <Form>
          <Form.Field required>
            <label>Usuario de STK</label>
            <input
              placeholder="Username"
              onChange={(e) => this.setState({ username: e.target.value })}
            />
          </Form.Field>
          <Form.Field required>
            <label>Ubicacion</label>
            <Button.Group>
              <Button
                positive={server === 'Brasil'}
                onClick={() => this.setState({ server: 'Brasil' })}
                content="Brasil"
              />
              <Button
                positive={server === 'Miami'}
                content="Miami"
                onClick={() => this.setState({ server: 'Miami' })}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>Sugerencias para campeonato</label>
            <TextArea
              onChange={(e) => this.setState({ sugerencias: e.target.value })}
            />
          </Form.Field>
          <Button
            type="submit"
            primary
            disabled={username.length === 0 || server.length === 0}
            onClick={async () => {
              await fetch('/api/respuestas', {
                method: 'POST',
                body: JSON.stringify({
                  formName, username, server, sugerencias, fecha: Date.now(),
                }),
              });
              this.setState({ done: true });
            }}
          >
            Submit
          </Button>
        </Form>
        <Modal
          open={this.state.done}
          size="small"
        >
          <Header icon>
            Gracias
          </Header>
          <Modal.Content>
            <p style={{ textAlign: 'center' }}>
              ¡Gracias! Nos vemos pronto
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={() => Router.push('/')} content="Ok" />
          </Modal.Actions>
        </Modal>
      </>
    );
  }
}

export const getServerSideProps: GetServerSideProps = async (context) => ({
  props: {
    ...(await serverSideTranslations(getLocale(context), ['common'])),
  },
});

export default withTranslation('common')(Register);
