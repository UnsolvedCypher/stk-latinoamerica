/* eslint-disable no-await-in-loop */
import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import SQL from 'sql-template-strings';

export default async (req, res) => {
  const db = await open({
    filename: `/home/${process.env.USER}/respuestas.db`,
    driver: sqlite3.Database,
  });

  const query = SQL`INSERT INTO respuesta VALUES (${req.body})`;
  await db.run(query);

  await db.close();
  res.statusCode = 200;
  res.json({});
};
